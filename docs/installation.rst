Installation
==========================

Introduction
------------

LSP is installed from a GIT repo on bitbucket.
Both Python 2.x and Python 3.x versions exist.

Python 2
--------
.. code-block:: python

    # Install git (if you don't already have it)
    sudo apt-get install git-core

    # Clone the repository to /home/pi/lightshowpi
    cd ~
    git clone https://togiles@bitbucket.org/togiles/lightshowpi.git

    # Grab the stable branch
    cd lightshowpi
    git fetch && git checkout stable

Python 3
--------
You must use the Python 3.x branch in the following cases:

* Raspberry Pi OS (formerly known as Raspbian) 'Buster' or later
* For RaspberryPi 4

.. code-block:: python

    sudo apt-get --allow-releaseinfo-change update
    sudo apt-get upgrade
    wget https://project-downloads.drogon.net/wiringpi-latest.deb
    sudo dpkg -i wiringpi-latest.deb
    sudo apt-get install git-core
    git clone https://togiles@bitbucket.org/togiles/lightshowpi.git
    cd lightshowpi
    git fetch && git checkout python3
    sudo ./install.sh
    sudo reboot 


