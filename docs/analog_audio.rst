Analog audio
============

The system can follow allow with any analog audio, such as the headphone output
from a PC/phone or from the Monitor output of your amplifier.
To do this, you will need to add an anaolgi input (mic input) to:

#.  Connect an external audio in sound card to the RaspberryPi
#.  Determine which analog input is the correct one (the RPi may detect multiple audio devices)
#.  Update overrides.cfg with the correct device

Soundcards
----------

something about USB sound cards

Use tools\find_audio.py
Set device ID as 3

Note, if you sometimes connect an external monitor HDMI wih internal speakers,
then the sound device list will renumerate, so the USB sound card you used while
headless may now be a different number.

sample 1:
tool output running headless

.. code-block:: python

    [1, 2]


sample 2:
Same output, but now with HDMI monitor connected.

.. code-block:: python

    [1, 2, 3]

Note the USB audio device is now device 3.


