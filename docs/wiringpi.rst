Raspberry Pi pin outputs
========================

LightshowPi uses WiringPi standards.
`<http://wiringpi.com/pins/>`_

======================== ========== ============
WiringPi/LightshowPi pin RPi Name   Physical pin
======================== ========== ============
0                        GPIO0      11
1                        GPIO1      12
2                        GPIO2      13
3                        GPIO3      15
4                        GPIO4      16
5                        GPIO5      18
6                        GPIO6      22
7                        GPIO7      7
8                        SDA        3
9                        SCL        5
10                       CE0        24
11                       CE01       26
12                       MOSI       19
13                       MISO       21
14                       SCLK       23
15                       TXD        8
16                       RXD        10
17                       GPIO8      Sec 3
18                       GPIO9      Sec 4
19                       GPIO10     Sec 5
20                       GPIO11     Sec 6
======================== ========== ============

Howto: Blinking LEDS
--------------------

The "hello world" of LightshowPi is to hook up a couple of LEDs and make them blink.
Have a read of this googledoc_, then you should be able to achieve the same results as shown below.

.. _googledoc: https://docs.google.com/document/d/1x97JIu5xVInZMutTNeaHlnQuyoLHjf3h-ugIo64pGfI

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/26Cs3lhwB1Q" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="//www.youtube.com/embed/cEi_ZcF2YM0" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    