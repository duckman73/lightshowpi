# lightshowpi documentation project

My attempt to collate a single location for all the info about this project.

Documents are created in .rst format

Use sphinx to generate the html.

    python -m pip install sphinx
    sphinx-quickstart

Follow the prompts.  Installer defaults are usually ok.
After each edit, recreate the html.

    ./make html

Output filea are in the /_build/html folder, and (in theory) ready to be linked to readthedocs.

