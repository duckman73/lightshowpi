lightshowpi  documentation
==========================

Blinky lights for Christmas, powered by Raspberry Pi.

Getting started
---------------

:doc:`Installation <installation>`

:doc:`Configuration <configuration>`

Input devices
-------------
(Methods to get audio into the system)

:doc:`Local Playlist <playlist>`

Music stored locally on the RPi and run in sequence, or randomy

:doc:`AirPlay <airplay>`

Connect to your wifi network and sync to AirPlay on devices on your network

:doc:`Analog audio <analog_audio>`

Connect a USB audio input card and match lighting to analog audio from any device

Output devices
--------------
Thinks that blink.

:doc:`Raspberry Pi output pins <wiringpi>`

:doc:`Arduino as an output expander <arduino>`

Ways to control LightShowPi
---------------------------

:doc:`Web browser interface <wiringpi>`

Utilities
---------
Tools that help debug the system.

LED test

Other resources
---------------

`Homepage <http://lightshowpi.org/>`_

`Reddit <https://www.reddit.com/r/LightShowPi/>`_

`Project source (bitbucket) <https://bitbucket.org/togiles/lightshowpi>`_

`Documentation source (bitbucket) <https://bitbucket.org/duckman73/lightshowpi>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   analog_audio
   configuration
   microweb
   wiringpi
   arduino
   airplay
   playlist



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
